#!/bin/bash
# execute with config desired e.g. ./new-machine.sh workstation.conf.yml

DOTBOT_PATH="dotbot/bin/dotbot"

if [[ ! -e $1 ]]; then
  echo "No config for $1"
  exit 1
else 
  echo "Using config $1"
fi

echo "Ensuring packages present..."
sudo apt -qqq install -y micro tmux zsh
CODE=$?
if [[ $CODE -ne 0 ]]; then
  echo "Failed to get packages!"
  exit $CODE
fi
echo "Packages present!"

if [[ "$SHELL" != "$(which zsh)" ]]; then
  echo "Shell is $SHELL rather than zsh! Setting shell to $(which zsh)"
  chsh -s $(which zsh)
fi

if [[ ! -e "$DOTBOT_PATH" ]]; then
  echo "Executable for dotbot not found! Getting submodule"
  git submodule init
  git submodule update
fi

if [[ ! -x "$DOTBOT_PATH" ]]; then
  echo "Executable for dotbot not executable! chmod time"
  chmod u+x "$DOTBOT_PATH"
fi

DOTBOT_COMMAND="$DOTBOT_PATH -c $1"

eval $DOTBOT_COMMAND

which starship &> /dev/null
CODE=$?
if [[ $CODE -ne 0 ]]; then
  echo "Starship not found! Installing..."
  curl -sS https://starship.rs/install.sh | sh
fi